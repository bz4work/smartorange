<?php

class App
{
    private $_size,$_array;

    public function getArray()
    {
        return $this->_array;
    }

    public function __construct($size)
    {
        $this->_size = $size;
        return $this->init();
    }

    public function init(){
        $handle = file_get_contents("https://ru.wikipedia.org/wiki/AC/DC");
        return $this->generateArray($handle);
    }

    private function generateArray($data){
        preg_match_all("/\d{1}/", $data, $out1);
        preg_match_all("/\d{2}/", $data, $out2);
        preg_match_all("/\d{3}/", $data, $out3);
        preg_match_all("/\d{4}/", $data, $out4);
        preg_match_all("/\d{5}/", $data, $out5);
        $final = array_merge($out5[0],$out3[0],$out1[0],$out4[0],$out2[0]);
        $this->_array = array_rand($final,$this->_size);
    }
}
